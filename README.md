# Warm Fridge

Work In Progress, temperature regulation with REST API and InfluxDB support for a warmable fridge. Made for Raspberry Pi

# Setup

## Hardware

Here is a list of what hardware you need to build a warm fridge:

- Any Raspberry Pi (I only tested on Zero but it should work on any other)
- An old fridge
- Any kind of heater you can make fit into your fridge
- A DS18B20 sensor and a 4.7K resistor
- Two relays
- Wires

You will need to plug correctly the DS18B20 sensor into the pins of the Pi (very simple, just throw it in your favorite search engine). The relays might be tricky to install as they are often made to work at 5V, and Pis are working at 3.3V. You will find the answer on the internet as well. You can plug them anywhere in the pins.

Once you plugged everything in your Raspberry Pi, find a way to replace your fridge's thermostat with the "cold" relay (I can't really help you as it is different for every fridge) and power your heater with the "hot" relay.

## Software

Just install requirements (requirements.txt): `pip install -r requirements.txt`

To run warm-fridge, cd into the root directory of the project and type `python WarmFridge/__init__.py`

To allow it to run as a service, put the following in `/etc/systemd/system/warm-fridge.service`

```
Description=WarmFridge
After=network.target

[Service]
User=pi 
ExecStart=python python /home/pi/warm-fridge/WarmFridge/__init__.py
Restart=always
RestartSec=3 

[Install]
WantedBy=multi-user.target
```

# Configuration

When you start warm-fridge for the first time, it will create a config file called `config.yml`. Edit this file to suit your needs.

# Usage

## Parameters

Regulation takes three different parameters:

- Target: the temperature you want to reach
- Error: the error you allow (i.e. cooler will start if temperature exceeds target + error)
- Hysteresis: the delta you want to respect when something is happening (i.e. when the cooler is on, it will only stop when the temperature gets down below temperature + error - hysteresis)

```
       Cooling +----+
               |    |
T+E   -     ___-__  |      |    |   _/
         __/   |  \ |      |    | _/
T+E-H - /      |   \|      |    |/
T     --------------\-----------/-------
T-E+H -        |    |\_    |   /|
               |    |  \__ |  / |
T-E   -        |    |     \__/  |
                           |    |
                           +----+ Warming
```

This scheme is obviously not ideal, you have to choose error and hysteresis in order to avoid inertia to make both of the devices running like this.

The only restrictions are:

- Target has to be between 2°C and 40°C
- Error has to be positive and lower than 10°C
- Hysteresis has to be between 0.1°C and 10°C
- Error has to be greater than hysteresis

## Values setting

Initial values are defined in the configuration, the all values can be changed with the REST API.

Here is the full list of the available routes:

- `/get`: get the temperature read from the sensor
- `/set-hot/<0,1>`: activate or deactivate the heater
- `/set-cold/<0,1>`: activate or deactivate the cooler
- `/set-regulation/<0,1>`: activate or deactivate the regulation
- `/set-target/<float>`: set the target temperature
- `/set-error/<float>`: set the error
- `/set-hysteresis/<float>`: set the hysteresis
- `/get-target`: get the target temperature
- `/is-regulating`: return a boolean according to regulation status
- `/is-warming`: return a boolean according to heater status
- `/is-cooling`: return a boolean according to cooler status

## InfluxDB

InfluxDB is supported out of the box, just configure your database in the config file and it will use it.

You will find two measurements with different values:

- Temperature:
  - target
  - value (the actual temperature)
  
- Regulation
  - regulation (is it regulating?)
  - warming (is it warming?)
  - cooling (is is cooling?)
  
The three lasts values are O or 1 to allow you to add or substract them for visualisation.

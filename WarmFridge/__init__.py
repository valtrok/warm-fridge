import atexit
import signal
from flask import Flask
from threading import Thread

from temp import Temp
from config import config
from regulation import RegulationThread

app = Flask(__name__)
rt = RegulationThread(config.init['target'], config.init['error'],
                      config.init['hysteresis'], config.delay,
                      config.init['regulation'], config.init['warming'],
                      config.init['cooling'])
temp = Temp()

server = Thread(
    target=app.run,
    kwargs={
        'host': config.api['host'],
        'port': config.api['port']
    })


@app.route('/get')
def get_temp():
    return str(temp.get())


@app.route('/set-hot/<int:onoff>')
def set_hot(onoff):
    if onoff in range(2):
        temp.set_hot(onoff == 1)
        return str(onoff == 1)
    return 'parameter has to be 0 or 1', 400


@app.route('/set-cold/<int:onoff>')
def set_cold(onoff):
    if onoff in range(2):
        temp.set_cold(onoff == 1)
        return str(onoff == 1)
    return 'parameter has to be 0 or 1', 400


@app.route('/set-regulation/<int:onoff>')
def set_regulation(onoff):
    if onoff in range(2):
        rt.regulation = (onoff == 1)
        return str(onoff == 1)
    return 'parameter has to be 0 or 1', 400


@app.route('/set-target/<float:target>')
@app.route('/set-target/<int:target>')
def set_target(target):
    if target >= 2 and target <= 40:
        rt.target = target
        return str(target)
    return 'target has to be between 2°C and 40°C', 400


@app.route('/set-error/<float:error>')
@app.route('/set-error/<int:error>')
def set_error(error):
    if error < 0 or error > 10:
        return 'error has to be between 0°C and 10°C', 400
    if error < rt.hysteresis:
        return 'error has to be greater than hysteresis', 400
    rt.error = error
    return str(error)


@app.route('/set-hysteresis/<float:hysteresis>')
@app.route('/set-hysteresis/<int:hysteresis>')
def set_hysteresis(hysteresis):
    if hysteresis < 0.1 or hysteresis > 10:
        return 'hysteresis has to be between 0.1°C and 10°C', 400
    if hysteresis > rt.error:
        return 'hysteresis has to be lesser than error', 400
    rt.hysteresis = hysteresis
    return str(hysteresis)


@app.route('/get-target')
def get_target():
    return str(rt.target)


@app.route('/is-regulating')
def is_regulating():
    return str(1 if rt.regulation else 0)


@app.route('/is-warming')
def is_warming():
    return str(temp.get_hot())


@app.route('/is-cooling')
def is_cooling():
    return str(temp.get_cold())


def finish_thread():
    rt.finish()
    print('Waiting for regulation thread to finish...')
    rt.join()
    print('Waiting for server process to finish...')
    print('Exiting.')


atexit.register(finish_thread)
signal.signal(signal.SIGTERM, finish_thread)

if __name__ == "__main__":
    rt.start()
    server.start()

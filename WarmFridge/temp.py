import RPi.GPIO as GPIO
from w1thermsensor import W1ThermSensor

from config import config

GPIO.setmode(GPIO.BOARD)
GPIO.setup(config.pins['heater'], GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(config.pins['cooler'], GPIO.OUT, initial=GPIO.HIGH)


class Temp:
    def __init__(self):
        self.sensor = W1ThermSensor()

    def get(self):
        return self.sensor.get_temperature()

    def set_hot(self, onoff):
        if self.get_cold():
            self.set_cold(False)
        GPIO.output(config.pins['heater'], GPIO.LOW if onoff else GPIO.HIGH)
        if self.get_hot():
            print('Warming')

    def set_cold(self, onoff):
        if self.get_hot():
            self.set_hot(False)
        GPIO.output(config.pins['cooler'], GPIO.LOW if onoff else GPIO.HIGH)
        if self.get_cold():
            print('Cooling')

    def get_hot(self):
        return not GPIO.input(config.pins['heater'])

    def get_cold(self):
        return not GPIO.input(config.pins['cooler'])

    def cleanup(self):
        GPIO.cleanup()

import time
import pytz
from datetime import datetime
from threading import Thread
from influxdb import InfluxDBClient

from config import config
from temp import Temp


class RegulationThread(Thread):
    def __init__(self,
                 target,
                 error,
                 hysteresis,
                 delay=10,
                 regulation=False,
                 warming=False,
                 cooling=False):
        super().__init__()
        self.temp = Temp()
        self.regulation = regulation
        self.old_regulation = regulation
        self.target = target
        self.error = error
        self.hysteresis = hysteresis
        self.delay = delay
        self.stop = False
        if not regulation:
            if warming:
                self.temp.set_hot(True)
            elif warming:
                self.temp.set_cold(True)
        if config.influxdb['enable']:
            if 'username' in config.influxdb.keys():
                self.idb_client = InfluxDBClient(
                    host=config.influxdb['host'],
                    port=config.influxdb['port'],
                    username=config.influxdb['username'],
                    password=config.influxdb['password'])
            else:
                self.idb_client = InfluxDBClient(
                    host=config.influxdb['host'], port=config.influxdb['port'])
            if config.influxdb['database'] not in [
                    d['name'] for d in self.idb_client.get_list_database()
            ]:
                self.idb_client.create_database(config.influxdb['database'])
            self.idb_client.switch_database(config.influxdb['database'])

    def push_to_influxdb(self, temperature):
        if config.influxdb['enable']:
            now = datetime.utcnow().replace(tzinfo=pytz.UTC).isoformat()
            json_body = [{
                "measurement": "temperature",
                "tags": {
                    "host": "warmfridge",
                    "region": "fr"
                },
                "time": now,
                "fields": {
                    "value": float(temperature),
                    "target": float(self.target)
                }
            }]
            self.idb_client.write_points(json_body)
            json_body = [{
                "measurement": "regulation",
                "tags": {
                    "host": "warmfridge",
                    "region": "fr"
                },
                "time": now,
                "fields": {
                    "regulation": 1 if self.regulation else 0,
                    "warming": 1 if self.temp.get_hot() else 0,
                    "cooling": 1 if self.temp.get_cold() else 0
                }
            }]
            self.idb_client.write_points(json_body)

    def run(self):
        while not self.stop:
            temperature = self.temp.get()
            if self.regulation:
                if self.temp.get_hot():
                    if temperature > self.target - self.error + self.hysteresis:
                        self.temp.set_hot(False)
                elif self.temp.get_cold():
                    if temperature < self.target + self.error - self.hysteresis:
                        self.temp.set_cold(False)
                else:
                    if temperature < self.target - self.error:
                        self.temp.set_hot(True)
                    elif temperature > self.target + self.error:
                        self.temp.set_cold(True)
            elif self.old_regulation:
                self.temp.set_cold(False)
                self.temp.set_hot(False)
            self.old_regulation = self.regulation
            self.push_to_influxdb(temperature)
            time.sleep(self.delay)
        self.temp.cleanup()

    def finish(self):
        self.stop = True
